//
//  ViewController.m
//  PixelBright
//
//  Created by Frank Ayars on 8/4/17.
//  Copyright © 2017 Plain Joe Studio. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@end

@implementation ViewController
@synthesize initialOrientation;
@synthesize currentOrientation;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    initialOrientation = UIInterfaceOrientationUnknown;
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(orientationChanged:)
     name:UIDeviceOrientationDidChangeNotification
     object:[UIDevice currentDevice]];

    [self updateDeviceOrientation];
    
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
    doubleTap.numberOfTapsRequired = 2;
    [self.view addGestureRecognizer:doubleTap];
    
    // Long press
    UILongPressGestureRecognizer* longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    longPress.delaysTouchesBegan = YES;
    [self.view addGestureRecognizer:longPress];
    
    [menuView initMenuView];
    [savedView initSavedView];
    colorView.controller = self;
    
    maskButton.contentMode = UIViewContentModeScaleAspectFit;
    maskButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentFill;
    maskButton.contentVerticalAlignment = UIControlContentVerticalAlignmentFill;
    
    menuButton.contentMode = UIViewContentModeScaleAspectFit;
    menuButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentFill;
    menuButton.contentVerticalAlignment = UIControlContentVerticalAlignmentFill;
    
}

-(void)updateDeviceOrientation
{
    UIInterfaceOrientation iOrientation = [UIApplication sharedApplication].statusBarOrientation;
    UIDeviceOrientation dOrientation = [UIDevice currentDevice].orientation;
    
    bool landscape;
    
    if (dOrientation == UIDeviceOrientationUnknown || dOrientation == UIDeviceOrientationFaceUp || dOrientation == UIDeviceOrientationFaceDown)
    {
        // If the device is laying down, use the UIInterfaceOrientation based on the status bar.
        landscape = UIInterfaceOrientationIsLandscape(iOrientation);
    }
    else
    {
        // If the device is not laying down, use UIDeviceOrientation.
        landscape = UIDeviceOrientationIsLandscape(dOrientation);
        
        // There's a bug in iOS!!!! http://openradar.appspot.com/7216046
        // So values needs to be reversed for landscape!
        if (dOrientation == UIDeviceOrientationLandscapeLeft)
        {
            iOrientation = UIInterfaceOrientationLandscapeRight;
        }
        else if (dOrientation == UIDeviceOrientationLandscapeRight)
        {
            iOrientation = UIInterfaceOrientationLandscapeLeft;
        }
        
        else if (dOrientation == UIDeviceOrientationPortrait)
        {
            iOrientation = UIInterfaceOrientationPortrait;
        }
        else if (dOrientation == UIDeviceOrientationPortraitUpsideDown)
        {
            iOrientation = UIInterfaceOrientationPortraitUpsideDown;
        }
    }
    
    if (landscape)
    {
        // Do stuff for landscape mode.
    }
    else
    {
        // Do stuff for portrait mode.
    }
    
    // Now manually rotate the view if needed.
    CGSize size = self.view.frame.size;
    switch (iOrientation)
    {
        case UIInterfaceOrientationPortraitUpsideDown:
        case UIInterfaceOrientationPortrait:
            {
                
                [pixelGridView.layer setValue:@(0) forKeyPath:@"transform.rotation.z"];
                pixelGridView.frame = CGRectMake(0, 0, MAX(size.width, size.height)-HEADER_SIZE, MIN(size.width, size.height));
                pixelGridView.center = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds));
                [pixelGridView.layer setValue:@(M_PI_2) forKeyPath:@"transform.rotation.z"];
                pixelGridView.frame = CGRectMake(0, HEADER_SIZE, pixelGridView.frame.size.width, pixelGridView.frame.size.height);
                
                header.frame = CGRectMake(0, 0, size.width, HEADER_SIZE);
                menuButton.frame = CGRectMake(8, 8, menuButton.frame.size.width, menuButton.frame.size.height);
                maskButton.frame = CGRectMake(size.width - maskButton.frame.size.width - 8, 8, maskButton.frame.size.width, maskButton.frame.size.height);
                
                gridSize.center = header.center;
            }
            break;
        case UIInterfaceOrientationLandscapeRight:
        case UIInterfaceOrientationLandscapeLeft:
            {
                pixelGridView.frame = CGRectMake(HEADER_SIZE, 0, MAX(size.width, size.height) - HEADER_SIZE, MIN(size.width, size.height));
                
                header.frame = CGRectMake(0, 0, HEADER_SIZE, size.height);
                menuButton.frame = CGRectMake(8, 8, menuButton.frame.size.width, menuButton.frame.size.height);
                
                maskButton.frame = CGRectMake(8, size.height - maskButton.frame.size.height - 8, maskButton.frame.size.width, maskButton.frame.size.height);
              
                gridSize.center = header.center;
            }
            break;
        default:
            break;
    }
    
    if(initialOrientation == UIInterfaceOrientationUnknown)
    {
        initialOrientation = iOrientation;
        
        switch (initialOrientation)
        {
            case UIInterfaceOrientationLandscapeLeft:
                rotationOffset = M_PI_2;
                break;

            case UIInterfaceOrientationLandscapeRight:
                rotationOffset = -M_PI_2;
                break;
                
            default:
                break;
        }
    }
}

-(void)viewDidLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    if([pixelGridView hasGrid] == false)
    {
        [pixelGridView newGridViewWithColumns:3 rows:2];
    }
    
    //[pixelGridView layoutPixels:NULL];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)newGridViewWithColumns:(int)columns rows:(int)rows
{
    //passthrough
    [pixelGridView newGridViewWithColumns:columns rows:rows];
}

-(void)adaptGridViewToColumns:(int)columns rows:(int)rows
{
    [pixelGridView drawGrid:0 row:0 columns:columns rows:rows];
}

-(IBAction)changeMask:(id)sender
{
    [pixelGridView saveGrid:false];
    
    [pixelGridView changeMask];
}

-(IBAction)menu:(id)sender
{
    [pixelGridView saveGrid:false];
    
    CGFloat width = self.view.frame.size.width;
    CGFloat height = self.view.frame.size.height;
    CGFloat xStart = 0;
    CGFloat yStart = 0;
    
    CGFloat angle = [(NSNumber *)[menuView.layer valueForKeyPath:@"transform.rotation.z"] floatValue];
    
    angle = ceilf(angle);
    NSLog(@"%f", angle);
    
    //handle the current coordinate system
    if(angle == 0)
    {
        /*
         0,0---x+
         |
         |
         y+
         */
        xStart = 0;
        yStart = -height;
        
        //default portrait
        CGFloat top = HEADER_SIZE;
        CGFloat left = (self.view.frame.size.width - menuView.container.frame.size.width)/2.0;
        
        if(UIInterfaceOrientationIsLandscape(currentOrientation) == true)
        {
            top = (self.view.frame.size.height - menuView.container.frame.size.height)/2.0;
            left = HEADER_SIZE;\
        }
        menuView.container.frame = CGRectMake(left, top, menuView.container.frame.size.width, menuView.container.frame.size.height);
    }
    else if(angle == -1)
    {
        /*
         y+--0,0
             |
             |
             x+
         */
        xStart = height*2;
        yStart = 0;
        
        //default portrait
        CGFloat top = HEADER_SIZE;
        CGFloat left = (self.view.frame.size.height - menuView.container.frame.size.width)/2.0;
        
        if(UIInterfaceOrientationIsLandscape(currentOrientation) == true)
        {
            top = (self.view.frame.size.width - menuView.container.frame.size.width)/2.0;
            left = self.view.frame.size.height - menuView.container.frame.size.width - HEADER_SIZE;
        }
        menuView.container.frame = CGRectMake(left, top, menuView.container.frame.size.width, menuView.container.frame.size.height);
    }
    else if(angle == 4)
    {
        /*
             y+
             |
             |
         x+--0,0
         */
        xStart = 0;
        yStart = height*2;
        
        //default portrait
        CGFloat top = self.view.frame.size.height - menuView.container.frame.size.height - HEADER_SIZE;
        CGFloat left = (self.view.frame.size.width - menuView.container.frame.size.width)/2.0;
        
        if(UIInterfaceOrientationIsLandscape(currentOrientation) == true)
        {
            top = (self.view.frame.size.height - menuView.container.frame.size.height)/2.0;
            left = self.view.frame.size.width - menuView.container.frame.size.width - HEADER_SIZE;
        }
        
        menuView.container.frame = CGRectMake(left, top, menuView.container.frame.size.width, menuView.container.frame.size.height);
    }
    else if(angle == 2)
    {
        /*
         x+
         |
         |
         0,0---y+
         */
        xStart = -height;
        yStart = 0;
        
        //default portrait
        CGFloat top = self.view.frame.size.width - menuView.container.frame.size.height - HEADER_SIZE;
        CGFloat left = (self.view.frame.size.height - menuView.container.frame.size.width)/2.0;
        
        if(UIInterfaceOrientationIsLandscape(currentOrientation) == true)
        {
            top = (self.view.frame.size.width - menuView.container.frame.size.width)/2.0;
            left = HEADER_SIZE;
        }
        menuView.container.frame = CGRectMake(left, top, menuView.container.frame.size.width, menuView.container.frame.size.height);
    }
    
    NSLog(@"%f %f", width, height);
    //[menuView setBackgroundColor:[UIColor blueColor]];
    menuView.frame = CGRectMake(xStart, yStart, width, height);
    menuView.hidden = FALSE;
    [UIView animateWithDuration:0.25 animations:^{
        menuView.frame = CGRectMake(0, 0, width, height);
    } ];
}

-(IBAction)closeMenu:(id)sender
{
    [UIView animateWithDuration:0.25 animations:^{
        
        menuView.frame = CGRectMake(0, 0, self.view.frame.size.width, 0);
        
    } completion:^(BOOL finished) {
        menuView.hidden = true;
    } ];
}

-(IBAction)openContextMenu:(id)sender
{
    [pixelGridView saveGrid:false];
    
    [contextMenuViewController.tableView reloadData];
    
    CGFloat width = self.view.frame.size.width;
    CGFloat height = self.view.frame.size.height;
    CGFloat xStart = 0;
    CGFloat yStart = 0;
    
    CGFloat angle = [(NSNumber *)[contextMenuView.layer valueForKeyPath:@"transform.rotation.z"] floatValue];
    
    angle = ceilf(angle);
    NSLog(@"%f", angle);
    
    //handle the current coordinate system
    if(angle == 0)
    {
        /*
         0,0---x+
         |
         |
         y+
         */
        xStart = 0;
        yStart = -height;
        
        //default portrait
        CGFloat top = HEADER_SIZE;
        CGFloat left = (self.view.frame.size.width - contextMenuView.container.frame.size.width)/2.0;
        
        if(UIInterfaceOrientationIsLandscape(currentOrientation) == true)
        {
            top = (self.view.frame.size.height - contextMenuView.container.frame.size.height)/2.0;
            left = HEADER_SIZE;\
        }
        contextMenuView.container.frame = CGRectMake(left, top, contextMenuView.container.frame.size.width, contextMenuView.container.frame.size.height);
    }
    else if(angle == -1)
    {
        /*
         y+--0,0
         |
         |
         x+
         */
        xStart = height*2;
        yStart = 0;
        
        //default portrait
        CGFloat top = HEADER_SIZE;
        CGFloat left = (self.view.frame.size.height - contextMenuView.container.frame.size.width)/2.0;
        
        if(UIInterfaceOrientationIsLandscape(currentOrientation) == true)
        {
            top = (self.view.frame.size.width - contextMenuView.container.frame.size.width)/2.0;
            left = self.view.frame.size.height - contextMenuView.container.frame.size.width - HEADER_SIZE;
        }
        contextMenuView.container.frame = CGRectMake(left, top, contextMenuView.container.frame.size.width, contextMenuView.container.frame.size.height);
    }
    else if(angle == 4)
    {
        /*
         y+
         |
         |
         x+--0,0
         */
        xStart = 0;
        yStart = height*2;
        
        //default portrait
        CGFloat top = self.view.frame.size.height - contextMenuView.container.frame.size.height - HEADER_SIZE;
        CGFloat left = (self.view.frame.size.width - contextMenuView.container.frame.size.width)/2.0;
        
        if(UIInterfaceOrientationIsLandscape(currentOrientation) == true)
        {
            top = (self.view.frame.size.height - contextMenuView.container.frame.size.height)/2.0;
            left = self.view.frame.size.width - contextMenuView.container.frame.size.width - HEADER_SIZE;
        }
        
        contextMenuView.container.frame = CGRectMake(left, top, contextMenuView.container.frame.size.width, contextMenuView.container.frame.size.height);
    }
    else if(angle == 2)
    {
        /*
         x+
         |
         |
         0,0---y+
         */
        xStart = -height;
        yStart = 0;
        
        //default portrait
        CGFloat top = self.view.frame.size.width - contextMenuView.container.frame.size.height - HEADER_SIZE;
        CGFloat left = (self.view.frame.size.height - contextMenuView.container.frame.size.width)/2.0;
        
        if(UIInterfaceOrientationIsLandscape(currentOrientation) == true)
        {
            top = (self.view.frame.size.width - contextMenuView.container.frame.size.width)/2.0;
            left = HEADER_SIZE;
        }
        contextMenuView.container.frame = CGRectMake(left, top, contextMenuView.container.frame.size.width, contextMenuView.container.frame.size.height);
    }
    
    NSLog(@"%f %f", width, height);
    contextMenuView.frame = CGRectMake(xStart, yStart, width, height);
    contextMenuView.hidden = FALSE;
    [UIView animateWithDuration:0.25 animations:^{
        contextMenuView.frame = CGRectMake(0, 0, width, height);
    } ];
}

-(IBAction)closeContextMenu:(id)sender
{
    [UIView animateWithDuration:0.25 animations:^{
        
        contextMenuView.frame = CGRectMake(0, 0, self.view.frame.size.width, 0);
        
    } completion:^(BOOL finished) {
        contextMenuView.hidden = true;
    } ];
}

-(IBAction)openSaved:(id)sender
{
    [pixelGridView saveGrid:false];
    
    [savedCollectionViewController.collectionView reloadData];
    
    CGFloat width = self.view.frame.size.width;
    CGFloat height = self.view.frame.size.height;
    CGFloat xStart = 0;
    CGFloat yStart = 0;
    
    CGFloat angle = [(NSNumber *)[savedView.layer valueForKeyPath:@"transform.rotation.z"] floatValue];

    angle = ceilf(angle);
    NSLog(@"%f", angle);
    
    //handle the current coordinate system
    if(angle == 0)
    {
        /*
             0,0---x+
             |
             |
             y+
         */
        xStart = -width;
        yStart = 0;
    }
    else if(angle == -1)
    {
        /*
             y+--0,0
                 |
                 |
                 x+
         */
        xStart = 0;
        yStart = width*2;
    }
    else if(angle == 4)
    {
        /*
                 y+
                 |
                 |
             x+--0,0
         */
        xStart = width*2;
        yStart = 0;
    }
    else if(angle == 2)
    {
        /*
             x+
             |
             |
             0,0---y+
         */
        xStart = 0;
        yStart = -width;
    }
    
    savedView.frame = CGRectMake(xStart, yStart, width, height);
    savedView.hidden = FALSE;
    [savedView setNeedsLayout];
    
    [UIView animateWithDuration:0.25 animations:^{
        savedView.frame = self.view.frame;
    } ];
}

-(IBAction)closeSaved:(id)sender
{
    [UIView animateWithDuration:0.25 animations:^{
        
        CGFloat width = self.view.frame.size.width;
        CGFloat height = self.view.frame.size.height;
        CGFloat xStart = 0;
        CGFloat yStart = 0;
        
        CGFloat angle = [(NSNumber *)[savedView.layer valueForKeyPath:@"transform.rotation.z"] floatValue];
        
        angle = ceilf(angle);
        NSLog(@"%f", angle);
        
        //handle the current coordinate system
        if(angle == 0)
        {
            /*
                 0,0---x+
                 |
                 |
                 y+
             */
            xStart = -width;
            yStart = 0;
        }
        else if(angle == -1)
        {
            /*
                 y+--0,0
                 |
                 |
                 x+
             */
            xStart = 0;
            yStart = width*2;
        }
        else if(angle == 4)
        {
            /*
                 y+
                 |
                 |
                 x+--0,0
             */
            xStart = width*2;
            yStart = 0;
        }
        else if(angle == 2)
        {
            /*
                 x+
                 |
                 |
                 0,0---y+
             */
            xStart = 0;
            yStart = -width;
        }
        
        savedView.frame = CGRectMake(xStart, yStart, width, height);
    } completion:^(BOOL finished) {
        savedView.hidden = true;
    } ];
}

-(void)closeColors
{
    colorView.hidden = true;
}

-(void)openGrid:(NSString*)fileName
{
    [pixelGridView openGrid:fileName];
}

-(void)openColors:(int)x y:(int)y
{
    CGFloat width = self.view.frame.size.width;
    //CGFloat height = self.view.frame.size.height;
    CGFloat xStart = 0;
    CGFloat yStart = 0;
    
    CGFloat angle = [(NSNumber *)[colorView.layer valueForKeyPath:@"transform.rotation.z"] floatValue];
    
    angle = ceilf(angle);
    NSLog(@"%f", angle);
    
    //handle the current coordinate system
    if(angle == 0)
    {
        /*
         0,0---x+
         |
         |
         y+
         */
        xStart = -width;
        yStart = 0;
    }
    else if(angle == -1)
    {
        /*
         y+--0,0
         |
         |
         x+
         */
        xStart = 0;
        yStart = width*2;
    }
    else if(angle == 4)
    {
        /*
         y+
         |
         |
         x+--0,0
         */
        xStart = width*2;
        yStart = 0;
    }
    else if(angle == 2)
    {
        /*
         x+
         |
         |
         0,0---y+
         */
        xStart = 0;
        yStart = -width;
    }

    [self.view bringSubviewToFront:colorView];
    colorView.frame = self.view.frame;
    CGPoint converted = [self.view convertPoint:CGPointMake(x, y) toView:colorView];
    [colorView position:converted.x y:converted.y];
    colorView.hidden = false;
}

-(void)handleDoubleTap:(UITapGestureRecognizer *)recognizer
{
    if(recognizer.state == UIGestureRecognizerStateRecognized)
    {
        CGPoint point = [recognizer locationInView:recognizer.view];
        CGPoint converted = [self.view convertPoint:point toView:pixelGridView];
        [pixelGridView handleDoubleTap:converted.x y:converted.y];
    }
}

-(void)handleLongPress:(UITapGestureRecognizer *)recognizer
{
    CGPoint point = [recognizer locationInView:recognizer.view];
    if(CGRectContainsPoint(pixelGridView.frame, point) == true)
    {
        if(recognizer.state == UIGestureRecognizerStateBegan)
        {
            CGPoint converted = [self.view convertPoint:point toView:pixelGridView];
            
            [pixelGridView setCurrentPixel:converted.x y:converted.y];
            
            colorView.frame = self.view.frame;
            
            [self openColors:point.x y:point.y];
            //[pixelGridView handleLongPress:converted.x y:converted.y];
        }
        
        if(recognizer.state == UIGestureRecognizerStateEnded)
        {
            CGPoint converted = [self.view convertPoint:point toView:colorView];
            [colorView pickColor:converted.x y:converted.y];
        }
    }
    else if(CGRectContainsPoint(menuButton.frame, point) == true)
    {
        if(recognizer.state == UIGestureRecognizerStateBegan)
        {
            [self openContextMenu:self];
        }
    }
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString * segueName = segue.identifier;
    if ([segueName isEqualToString: @"SavedCollectionViewSegue"])
    {
        savedCollectionViewController = (SavedCollectionViewController *) [segue destinationViewController];
        savedCollectionViewController.controller = self;
    }
    else if ([segueName isEqualToString: @"TemplatesCollectionViewSegue"])
    {
        templatesCollectionViewController = (TemplatesCollectionViewController*) [segue destinationViewController];
        templatesCollectionViewController.controller = self;
    }
    else if ([segueName isEqualToString: @"MenuViewSegue"])
    {
        menuViewController = (MenuViewController*) [segue destinationViewController];
        menuViewController.controller = self;
    }
    else if ([segueName isEqualToString: @"ContextMenuViewSegue"])
    {
        contextMenuViewController = (ContextMenuViewController*) [segue destinationViewController];
        contextMenuViewController.controller = self;
    }
}

-(void)setGridSize:(int)columnCount rowCount:(int)rowCount
{
    [gridSize setTitle:[NSString stringWithFormat:@"%dx%d", columnCount, rowCount] forState:UIControlStateNormal];
}

-(void)setCurrentPixelHue:(float)hue saturation:(float)saturation value:(float)value
{
    [pixelGridView setCurrentPixelHue:hue saturation:saturation value:value];
}

-(void)share
{
    
    if(pixelGridView != NULL)
    {
        NSString* filename = [pixelGridView getFilename:@"png"];
        
        NSString *textToShare = @"A new creation from Pixel Bright on iOS!";
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *cachePath = [paths objectAtIndex:0];
        NSString *filePath =  [cachePath stringByAppendingPathComponent:filename];
        
        UIImage* image = [UIImage imageWithContentsOfFile:filePath];
        
        NSArray *objectsToShare = @[textToShare, image];
        
        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
        
        NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                       UIActivityTypePrint,
                                       UIActivityTypeAssignToContact,
                                       UIActivityTypeAddToReadingList,
                                       UIActivityTypePostToFlickr,
                                       UIActivityTypePostToVimeo];
        
        activityVC.excludedActivityTypes = excludeActivities;
        
        [self presentViewController:activityVC animated:YES completion:nil];
    }
}

-(BOOL)shouldAutorotate
{
    return FALSE;
}

- (void) orientationChanged:(NSNotification *)note
{
    UIDevice * device = note.object;
    currentOrientation = (UIInterfaceOrientation)device.orientation;
    
    [self orientIcons:device.orientation];
}

-(void)orientIcons:(UIDeviceOrientation)orientation
{
    CGFloat angle = 0;
    switch(orientation)
    {
        case UIDeviceOrientationPortrait:
        {
            angle = rotationOffset;
            break;
        }
        case UIDeviceOrientationPortraitUpsideDown:
        {
            angle = M_PI+rotationOffset;
            break;
        }
        case UIDeviceOrientationLandscapeLeft:
        {
            angle = M_PI_2+rotationOffset;
            break;
        }
        case UIDeviceOrientationLandscapeRight:
        {
            angle = -M_PI_2+rotationOffset;
            break;
        }
        default:
            break;
    };
    
    CGFloat width = self.view.frame.size.width;
    CGFloat height = self.view.frame.size.height;
    
    [UIView animateWithDuration:0.25 animations:^{
        [gridSize.layer setValue:@(angle) forKeyPath:@"transform.rotation.z"];
        [menuButton.layer setValue:@(angle) forKeyPath:@"transform.rotation.z"];
        [maskButton.layer setValue:@(angle) forKeyPath:@"transform.rotation.z"];
        
        [savedView.layer setValue:@(angle) forKeyPath:@"transform.rotation.z"];
        [templatesView.layer setValue:@(angle) forKeyPath:@"transform.rotation.z"];
        [menuView.layer setValue:@(angle) forKeyPath:@"transform.rotation.z"];
        [contextMenuView.layer setValue:@(angle) forKeyPath:@"transform.rotation.z"];
        [colorView.layer setValue:@(angle) forKeyPath:@"transform.rotation.z"];
        
        if(savedView.hidden == false)
        {
            savedView.frame = CGRectMake(0, 0, width, height);
        }
        
        if(templatesView.hidden == false)
        {
            templatesView.frame = CGRectMake(0, 0, width, height);
        }
        
        if(menuView.hidden == false)
        {
            menuView.frame = CGRectMake(0, 0, width, height);
        }
        
        if(contextMenuView.hidden == false)
        {
            contextMenuView.frame = CGRectMake(0, 0, width, height);
        }
        
        if(colorView.hidden == false)
        {
            colorView.frame = CGRectMake(0, 0, width, height);
        }
        
        if(templatesView.hidden == false)
        {
            templatesView.frame = CGRectMake(0, 0, width, height);
        }
    }];
}

-(IBAction)deleteGrid:(id)sender
{
    [pixelGridView deleteGrid];
    [pixelGridView newGridViewWithColumns:3 rows:2];
}

-(IBAction)saveAsTemplate:(id)sender
{
    [pixelGridView saveGrid:true];
}

-(IBAction)openTemplates:(id)sender
{
    [pixelGridView saveGrid:false];
    
    [templatesCollectionViewController.collectionView reloadData];
    
    CGFloat width = self.view.frame.size.width;
    CGFloat height = self.view.frame.size.height;
    CGFloat xStart = 0;
    CGFloat yStart = 0;
    
    CGFloat angle = [(NSNumber *)[templatesView.layer valueForKeyPath:@"transform.rotation.z"] floatValue];
    
    angle = ceilf(angle);
    NSLog(@"%f", angle);
    
    //handle the current coordinate system
    if(angle == 0)
    {
        /*
         0,0---x+
         |
         |
         y+
         */
        xStart = -width;
        yStart = 0;
    }
    else if(angle == -1)
    {
        /*
         y+--0,0
         |
         |
         x+
         */
        xStart = 0;
        yStart = width*2;
    }
    else if(angle == 4)
    {
        /*
         y+
         |
         |
         x+--0,0
         */
        xStart = width*2;
        yStart = 0;
    }
    else if(angle == 2)
    {
        /*
         x+
         |
         |
         0,0---y+
         */
        xStart = 0;
        yStart = -width;
    }
    
    templatesView.frame = CGRectMake(xStart, yStart, width, height);
    templatesView.hidden = FALSE;
    [templatesView setNeedsLayout];
    
    [UIView animateWithDuration:0.25 animations:^{
        templatesView.frame = self.view.frame;
    } ];
}

-(IBAction)closeTemplates:(id)sender
{
    [UIView animateWithDuration:0.25 animations:^{
        
        CGFloat width = self.view.frame.size.width;
        CGFloat height = self.view.frame.size.height;
        CGFloat xStart = 0;
        CGFloat yStart = 0;
        
        CGFloat angle = [(NSNumber *)[templatesView.layer valueForKeyPath:@"transform.rotation.z"] floatValue];
        
        angle = ceilf(angle);
        NSLog(@"%f", angle);
        
        //handle the current coordinate system
        if(angle == 0)
        {
            /*
             0,0---x+
             |
             |
             y+
             */
            xStart = -width;
            yStart = 0;
        }
        else if(angle == -1)
        {
            /*
             y+--0,0
             |
             |
             x+
             */
            xStart = 0;
            yStart = width*2;
        }
        else if(angle == 4)
        {
            /*
             y+
             |
             |
             x+--0,0
             */
            xStart = width*2;
            yStart = 0;
        }
        else if(angle == 2)
        {
            /*
             x+
             |
             |
             0,0---y+
             */
            xStart = 0;
            yStart = -width;
        }
        
        templatesView.frame = CGRectMake(xStart, yStart, width, height);
    } completion:^(BOOL finished) {
        templatesView.hidden = true;
    } ];
}

-(void)setLandscape:(bool)isLandscape
{
    if(pixelGridView != NULL)
    {
        [pixelGridView setLandscape:isLandscape];
    }
}

-(bool)isLandscape
{
    bool landscape = true;
    
    if(pixelGridView != NULL)
    {
        landscape = [pixelGridView isLandscape];
    }
    
    return landscape;
}

-(void)saveToPhotos
{
    if(pixelGridView != NULL)
    {
        [pixelGridView saveToPhotos];
    }
}

@end
