//
//  PixelGridView.h
//  PixelBright
//
//  Created by Frank Ayars on 8/4/17.
//  Copyright © 2017 Plain Joe Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PixelView.h"
#import "PixelGrid.h"

#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)
#define DELAY_SECONDS 0.1
#define ASPECT_RATIO 1.5
#define HEADER_SIZE 60
#define MAX_WORKING_PIXELS_COLUMNS 24
#define MAX_WORKING_PIXELS_ROWS 16

@class ViewController;

@interface PixelGridView : UIView
{
    NSUUID* gridId;
    
    bool landscape;
    
    UITouch* firstTouch;
    UITouch* secondTouch;
    
    NSMutableArray* pixels;
    NSMutableArray* templatePixels;
    
    PixelView* currentPixelView;
    
    int maskId;
    
    int startColumn;
    int startRow;
    int rowCount;
    int columnCount;

    IBOutlet ViewController* controller;
    
    NSDate* dateCreated;
    NSDate* dateModified;
    
    NSDate* touchTimestamp;
    
    //TODO CONFIG: CGFloat* currentHSV;
}

-(void)newGridViewWithColumns:(int)columns rows:(int)rows;
-(void)openGrid:(NSString*) fileName;
-(bool)hasBeenTouched;
-(void)saveGrid:(bool)asTemplate;
-(void)changeMask;
-(void)handleDoubleTap:(int)x y:(int)y;
-(void)handleLongPress:(int)x y:(int)y;
-(PixelView*)setCurrentPixel:(int)x y:(int)y;
-(void)setCurrentPixelHue:(float)hue saturation:(float)saturation value:(float)value;
-(bool)hasGrid;
-(bool)deleteGrid;
-(NSString*)getFilename:(NSString*)extension;
-(void)setLandscape:(bool)landscapeMode;
-(bool)isLandscape;
-(void)saveToPhotos;
-(void)drawGrid:(int)column row:(int)row columns:(int)columns rows:(int)rows;

@end
