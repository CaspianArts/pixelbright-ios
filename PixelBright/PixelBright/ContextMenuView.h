//
//  Menu.h
//  PixelBright
//
//  Created by Frank Ayars on 8/15/17.
//  Copyright © 2017 Plain Joe Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface ContextMenuView : UIView
{
    IBOutlet ViewController* controller;
    
}

@property (nonatomic) IBOutlet UIView* container;

-(IBAction)deleteGrid:(id)sender;
-(IBAction)saveAsTemplate:(id)sender;
-(IBAction)openTemplate:(id)sender;

@end
