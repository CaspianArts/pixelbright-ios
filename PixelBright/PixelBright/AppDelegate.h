//
//  AppDelegate.h
//  PixelBright
//
//  Created by Frank Ayars on 8/4/17.
//  Copyright © 2017 Plain Joe Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

