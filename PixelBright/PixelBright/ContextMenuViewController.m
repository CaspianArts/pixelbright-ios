//
//  TemplatesCollectionView.m
//  PixelBright
//
//  Created by Frank Ayars on 10/31/17.
//  Copyright © 2017 Plain Joe Studio. All rights reserved.
//

#import "ContextMenuViewController.h"
#import "ViewController.h"

@implementation ContextMenuViewController

@synthesize controller;

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //always rebuild (landscape/portrait) may change
    menuOptions = [[NSMutableArray alloc] init];
    [menuOptions addObject:@"Share"];
    [menuOptions addObject:@"New from Template"];
    [menuOptions addObject:@"Save as Template"];
    [menuOptions addObject:@"Save to Photos"];
    if([controller isLandscape] == true)
    {
        [menuOptions addObject:@"Portrait"];
    }
    else
    {
        [menuOptions addObject:@"Landscape"];
    }
    [menuOptions addObject:@"Delete"];
    
    return menuOptions.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    UILabel* title = (UILabel *)[cell viewWithTag:100];
    [title setText:[menuOptions objectAtIndex:indexPath.row]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)
    {
        //share...
        [controller share];
    }
    else if(indexPath.row == 1)
    {
        [controller openTemplates:self];
    }
    else if(indexPath.row == 2)
    {
        [controller saveAsTemplate:self];
    }
    else if(indexPath.row == 3)
    {
        [controller saveToPhotos];
    }
    else if(indexPath.row == 4)
    {
        //toggle
        bool landscape = [controller isLandscape] != true;
        [controller setLandscape:landscape];
    }
    else if(indexPath.row == 5)
    {
        //delete the current grid
        [controller deleteGrid:self];
    }
    
    [controller closeContextMenu:self];
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:true];
}

@end
