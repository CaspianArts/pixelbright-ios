//
//  ColorView.h
//  PixelBright
//
//  Created by Frank Ayars on 12/20/17.
//  Copyright © 2017 Plain Joe Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface ColorView : UIView
{
    IBOutlet UIImageView* colorWheel;
}

@property (nonatomic) ViewController* controller;

-(void)position:(int)x y:(int)y;
-(void)pickColor:(int)x y:(int)y;

@end
