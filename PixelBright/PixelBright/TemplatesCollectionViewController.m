//
//  TemplatesCollectionView.m
//  PixelBright
//
//  Created by Frank Ayars on 10/31/17.
//  Copyright © 2017 Plain Joe Studio. All rights reserved.
//

#import "TemplatesCollectionViewController.h"
#import "ViewController.h"

@implementation TemplatesCollectionViewController

@synthesize controller;

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString* path = [paths objectAtIndex:0];
    
    NSArray* directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:NULL];

    NSMutableArray* tempFiles = [[NSMutableArray alloc] init];
    
    for (NSString* item in directoryContent)
    {
        if ([[item pathExtension] isEqualToString:@"tmpl"])
        {
            [tempFiles addObject:item];
        }
    }
    
    NSArray* sortedFilelist;
    sortedFilelist = [tempFiles sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2)
    {
        NSDictionary* grid1 = [NSDictionary dictionaryWithContentsOfFile:[NSString stringWithFormat:@"%@/%@", path, obj1]];
        NSDate* first = [grid1  objectForKey:@"dateModified"];
        NSDictionary* grid2 = [NSDictionary dictionaryWithContentsOfFile:[NSString stringWithFormat:@"%@/%@", path, obj2]];
        NSDate* second = [grid2 objectForKey:@"dateModified"];
        return [second compare:first];
    }];
    
    files = sortedFilelist;
    
    return files.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"Cell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    UILabel* title = (UILabel *)[cell viewWithTag:100];
    UIImageView* thumbView = (UIImageView *)[cell viewWithTag:101];
    thumbView.layer.cornerRadius = 3;
    thumbView.clipsToBounds = YES;
    
    UIView* roundedCornerFrameWhite = (UIView*)[cell viewWithTag:102];
    roundedCornerFrameWhite.layer.cornerRadius = 3;
    roundedCornerFrameWhite.clipsToBounds = YES;
    
    UIView* roundedCornerFrameBlack = (UIView*)[cell viewWithTag:103];
    roundedCornerFrameBlack.layer.cornerRadius = 3;
    roundedCornerFrameBlack.clipsToBounds = YES;
    
    NSString* fileName = [files objectAtIndex:indexPath.row];
    
    NSString* thumbFileName = [fileName stringByDeletingPathExtension];
    thumbFileName = [NSString stringWithFormat:@"%@-tmpl.png", thumbFileName];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachePath = [paths objectAtIndex:0];
    NSString* thumbFilePath =  [cachePath stringByAppendingPathComponent:thumbFileName];
    
    UIImage* image = [UIImage imageWithContentsOfFile:thumbFilePath];
    thumbView.image = image;
    
    int boardNum = (int)(files.count - indexPath.row);
    NSString* subTitle = [NSString stringWithFormat:@"BOARD #%d", boardNum];
    [title setText:subTitle];
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* fileName = [files objectAtIndex:indexPath.row];
    
    [controller openGrid:fileName];
    [controller closeTemplates:self];
}

@end
