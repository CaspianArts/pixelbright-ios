//
//  Menu.h
//  PixelBright
//
//  Created by Frank Ayars on 8/15/17.
//  Copyright © 2017 Plain Joe Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface MenuView : UIView
{
    IBOutlet UIButton* button1x1;
    IBOutlet UIButton* button1x2;
    IBOutlet UIButton* button2x3;
    IBOutlet UIButton* button4x6;
    IBOutlet UIButton* button8x12;
    IBOutlet UIButton* button16x24;
    IBOutlet UIButton* buttonSaved;
    IBOutlet UIImageView* logoView;
    
    IBOutlet ViewController* controller;
    
    //IBOutlet UIImageView* mask;
    
}

@property (nonatomic) IBOutlet UIView* container;

-(void)initMenuView;
-(IBAction)newGrid:(id)sender;
-(IBAction)openGrid:(id)sender;

@end
