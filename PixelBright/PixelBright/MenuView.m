//
//  Menu.m
//  PixelBright
//
//  Created by Frank Ayars on 8/15/17.
//  Copyright © 2017 Plain Joe Studio. All rights reserved.
//

#import "MenuView.h"
#import "ViewController.h"

@implementation MenuView

@synthesize container;

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
/*- (void)drawRect:(CGRect)rect
{
    
}
*/

-(void)initMenuView
{
    /*CALayer* maskLayer = [CALayer layer];
    maskLayer.contents = (id)mask.image.CGImage;
    maskLayer.frame = CGRectMake(0, 0, mask.frame.size.width, mask.frame.size.height);
    container.layer.mask = maskLayer;
    container.layer.masksToBounds = YES;
    
    [self gradeView];
    [self gradeButtons];
    
    CGFloat left = (((self.frame.size.width - 60) - logoView.frame.size.width)/2.0);
    logoView.frame = CGRectMake(left, logoView.frame.origin.y, logoView.frame.size.width, logoView.frame.size.height);
    */
}

-(void)layoutSubviews
{
    //CGFloat menuLeft = (self.frame.size.width - container.frame.size.width)/2.0;
    //container.frame = CGRectMake(menuLeft-20, container.frame.origin.y, container.frame.size.width, container.frame.size.height);
}

-(void)gradeView
{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    CGRect gradeBounds = [container bounds];
    gradeBounds.size.width = gradeBounds.size.width;
    gradient.frame = gradeBounds;
    gradient.startPoint = CGPointMake(0, 1);
    gradient.endPoint = CGPointMake(.5, 0);
    
    gradient.colors = @[(id)[UIColor colorWithRed:0xc3/255.0 green:0x30/255.0 blue:0xad/255.0 alpha:1.0].CGColor, (id)[UIColor colorWithRed:0x7f/255.0 green:0x30/255.0 blue:0xf3/255.0 alpha:1.0].CGColor];
    
    [container.layer insertSublayer:gradient atIndex:0];
    
}

-(void)gradeButtons
{
    [button2x3.layer insertSublayer:[self getButtonGradientLayer] atIndex:0];
    [button4x6.layer insertSublayer:[self getButtonGradientLayer] atIndex:0];
    [button8x12.layer insertSublayer:[self getButtonGradientLayer] atIndex:0];
    [button16x24.layer insertSublayer:[self getButtonGradientLayer] atIndex:0];
    [buttonSaved.layer insertSublayer:[self getButtonGradientLayer] atIndex:0];
}

-(CAGradientLayer*)getButtonGradientLayer
{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = button2x3.bounds;
    gradient.startPoint = CGPointMake(0, 0);
    gradient.endPoint = CGPointMake(1, 0);
    
    gradient.colors = @[(id)[UIColor whiteColor].CGColor, (id)[UIColor clearColor].CGColor];
    
    return gradient;
}

-(IBAction)newGrid:(id)sender
{
    if(sender == button1x1)
    {
        [controller adaptGridViewToColumns:1 rows:1];
        //[controller newGridViewWithColumns:1 rows:1];
    }
    else if(sender == button1x2)
    {
        [controller adaptGridViewToColumns:2 rows:1];
        //[controller newGridViewWithColumns:2 rows:1];
    }
    else if(sender == button2x3)
    {
        [controller adaptGridViewToColumns:3 rows:2];
        //[controller newGridViewWithColumns:3 rows:2];
    }
    else if(sender == button4x6)
    {
        [controller adaptGridViewToColumns:6 rows:4];
        //[controller newGridViewWithColumns:6 rows:4];
    }
    else if(sender == button8x12)
    {
        [controller adaptGridViewToColumns:12 rows:8];
        //[controller newGridViewWithColumns:12 rows:8];
    }
    else if(sender == button16x24)
    {
        [controller adaptGridViewToColumns:24 rows:16];
        //[controller newGridViewWithColumns:24 rows:16];
    }
    [controller closeMenu:self];
}

-(IBAction)openGrid:(id)sender
{
    
}

@end
