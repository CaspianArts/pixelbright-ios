//
//  SavedView.m
//  PixelBright
//
//  Created by Frank Ayars on 10/30/17.
//  Copyright © 2017 Plain Joe Studio. All rights reserved.
//

#import "SavedView.h"
#import "ViewController.h"

@implementation SavedView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)initSavedView
{
}

-(void)layoutSubviews
{
    CGFloat width = controller.view.frame.size.width;
    CGFloat height = controller.view.frame.size.height;
    if(UIInterfaceOrientationIsLandscape(controller.currentOrientation) == true)
    {
        width = MAX(controller.view.frame.size.width, controller.view.frame.size.height);
        height = MIN(controller.view.frame.size.width, controller.view.frame.size.height);
    }
    else
    {
        width = MIN(controller.view.frame.size.width, controller.view.frame.size.height);
        height = MAX(controller.view.frame.size.width, controller.view.frame.size.height);
    }
    bg.frame = CGRectMake(0, 0, width, height);
    collectionContainer.frame = CGRectMake(0, 60, width, height-60);
    NSLog(@"ls %f %f", width, height);
    
    CGFloat left = ((collectionContainer.frame.size.width - savedLabel.frame.size.width)/2.0);
    savedLabel.frame = CGRectMake(left, savedLabel.frame.origin.y, savedLabel.frame.size.width, savedLabel.frame.size.height);
    
    [self gradeView];
}

-(void)gradeView
{
    if(gradient != NULL)
    {
        [gradient removeFromSuperlayer];
    }
    
    gradient = [CAGradientLayer layer];

    gradient.frame = CGRectMake(0, 0, collectionContainer.frame.size.width, collectionContainer.frame.size.height + 60);
    gradient.startPoint = CGPointMake(0, 1);
    gradient.endPoint = CGPointMake(.5, 0);
    //eb7e6d
    //bf356f
    gradient.colors = @[(id)[UIColor colorWithRed:0xeb/255.0 green:0x7e/255.0 blue:0x6d/255.0 alpha:1.0].CGColor, (id)[UIColor colorWithRed:0xbf/255.0 green:0x35/255.0 blue:0x6f/255.0 alpha:1.0].CGColor];
    
    [bg.layer insertSublayer:gradient atIndex:0];
    
}

@end
