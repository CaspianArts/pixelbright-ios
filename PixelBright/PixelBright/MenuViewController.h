//
//  TemplatesCollectionView.h
//  PixelBright
//
//  Created by Frank Ayars on 10/31/17.
//  Copyright © 2017 Plain Joe Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;
@interface MenuViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray* menuOptions;
}

@property (nonatomic) ViewController* controller;

@end
