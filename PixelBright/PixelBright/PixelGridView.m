//
//  PixelGridView.m
//  PixelBright
//
//  Created by Frank Ayars on 8/4/17.
//  Copyright © 2017 Plain Joe Studio. All rights reserved.
//

#import "PixelGridView.h"
#import "ViewController.h"

@implementation PixelGridView

-(void)preparePixels:(bool)refresh
{
    if(pixels != NULL)
    {
        //remove existing views from the grid
        for(int pixel=0; pixel<pixels.count; pixel++)
        {
            PixelView* pixelView = [pixels objectAtIndex:pixel];
            [pixelView removeFromSuperview];
        }
    }
    
    if(pixels == NULL || refresh == true)
    {
        pixels = [[NSMutableArray alloc] init];
        int pixelCount = MAX_WORKING_PIXELS_ROWS*MAX_WORKING_PIXELS_COLUMNS;
        for(int pixel = 0; pixel < pixelCount; pixel++)
        {
            PixelView* view = [[PixelView alloc] init];
            [view initPixelView];
            UIImage* maskImage = [self getMaskImage];
            [view setMaskImage:maskImage];
            [pixels addObject:view];
        }
    }
}

-(void)drawGrid:(int)column row:(int)row columns:(int)columns rows:(int)rows
{
    [self preparePixels:false];
    
    startRow = row;
    startColumn = column;
    rowCount = rows;
    columnCount = columns;
    
    [controller setGridSize:columnCount rowCount:rowCount];
    
    //create the grid based on columns, rows
    CGSize size = [[UIScreen mainScreen] applicationFrame].size;
    size.height = size.height - HEADER_SIZE;
    
    //for free-design, always based on device landscape
    CGFloat gridWidth = MAX(self.frame.size.width,self.frame.size.height);
    CGFloat gridHeight = MIN(self.frame.size.width,self.frame.size.height);
    
    //square pixels (fit to width)
    int pixelWidth = (int)(gridWidth/columns);
    int pixelHeight = (int)(gridHeight/rows);
    int maxSize = MIN(pixelWidth, pixelHeight);
    pixelWidth = maxSize;
    pixelHeight = maxSize;
    
    CGFloat xOverflow = (gridWidth - (pixelWidth*(int)columns));
    CGFloat xOffset = xOverflow/2.0;
    
    CGFloat yOverflow = (gridHeight - (pixelHeight*(int)rows));
    CGFloat yOffset = yOverflow/2.0;
    
    //validations (must not start or cover unbounded area)
    //...
    for(int r=row; r<rows; r++)
    {
        for(int c=column; c<columns; c++)
        {
            int index = (r*MAX_WORKING_PIXELS_COLUMNS)+c;
            
            PixelView* pixelView = [pixels objectAtIndex:index];
            
            //we found the view, now size and place on screen
            int screenCol = c - column;
            int screenRow = r - row;
            CGRect pixelFrame = CGRectMake((pixelWidth*screenCol)+xOffset, (pixelHeight*screenRow)+yOffset, pixelWidth, pixelHeight);
            pixelView.frame = pixelFrame;
            [self addSubview:pixelView];
        }
    }
}
    
-(void)colorPixels:(NSArray*)savedPixels
{
    if(savedPixels != NULL)
    {
        NSDictionary* pixelDict = NULL;
        
        int pixelCount = MAX_WORKING_PIXELS_ROWS*MAX_WORKING_PIXELS_COLUMNS;
        for(int pixel = 0; pixel < pixelCount; pixel++)
        {
        
            PixelView* pixelView = [pixels objectAtIndex:pixel];
            pixelDict = [savedPixels objectAtIndex:pixel];
            
            if(pixelDict != NULL)
            {
                float hue = [[pixelDict objectForKey:@"hue"] floatValue];
                float saturation = [[pixelDict objectForKey:@"saturation"] floatValue];
                float value = [[pixelDict objectForKey:@"value"] floatValue];
                NSString* code = [pixelDict objectForKey:@"code"];
                bool touched = [[pixelDict objectForKey:@"touched"] boolValue];
                
                if(templatePixels == NULL)
                {
                    if(code != NULL && code.length > 0) //is not a template, but was generated from one...
                    {
                        //show the code
                        [pixelView setPixelCode:code];
                        [pixelView setHidden:false];
                    }
                    
                    if(touched == true)
                    {
                        [pixelView showPixel];
                        [pixelView setHSV:hue saturation:saturation value:value];
                    }
                }
                else //must be a template
                {
                    if(touched == true)
                    {
                        [pixelView setCode:hue saturation:saturation value:value];
                        [pixelView setHidden:false];
                    }
                }
            }
        }
    }
}
    
-(void)newGridViewWithColumns:(int)columns rows:(int)rows
{
    gridId = [NSUUID UUID];
    
    landscape = true;
    
    dateCreated = [NSDate date];
    
    //default
    maskId = 1;
    
    startColumn = 0;
    startRow = 0;
    columnCount = columns;
    rowCount = rows;
    
    [controller setGridSize:columnCount rowCount:rowCount];
    
    [self preparePixels:true];
    
    [self drawGrid:0 row:0 columns:columns rows:rows];
}

-(bool)hasGrid
{
    return (gridId != NULL);
}

-(void) openGrid:(NSString*) fileName
{
    [self preparePixels:true];
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString* cachePath = [paths objectAtIndex:0];
    NSString* filePath =  [cachePath stringByAppendingPathComponent:fileName];
    NSString* extension = [filePath pathExtension];
    
    NSDictionary* pixelsToOpen = [NSDictionary dictionaryWithContentsOfFile:filePath];

    NSString* gridIdString = [pixelsToOpen objectForKey:@"id"];
    gridId = [[NSUUID alloc] initWithUUIDString:gridIdString];
    maskId = [[pixelsToOpen objectForKey:@"maskId"] intValue];
    dateCreated = [pixelsToOpen objectForKey:@"dateCreated"];
    dateModified = [pixelsToOpen objectForKey:@"dateModified"];
    landscape = [[pixelsToOpen objectForKey:@"landscape"] boolValue];
    templatePixels = NULL;
    
    startColumn = [[pixelsToOpen objectForKey:@"startColumn"] intValue];;
    startRow = [[pixelsToOpen objectForKey:@"startRow"] intValue];
    columnCount = [[pixelsToOpen objectForKey:@"columnCount"] intValue];;
    rowCount = [[pixelsToOpen objectForKey:@"rowCount"] intValue];
    
    [controller setGridSize:columnCount rowCount:rowCount];
    
    NSArray* savedPixels = [pixelsToOpen objectForKey:@"pixels"];
    
    if([extension isEqualToString:@"tmpl"] == true)
    {
        templatePixels = [[NSMutableArray alloc] init];
        //create new grid from the template...
        gridId = [NSUUID UUID];
        dateCreated = [NSDate date];
        dateModified = [NSDate date];
    }
    
    [self colorPixels:savedPixels];
    [self executeMask];
    [self drawGrid:startColumn row:startRow columns:columnCount rows:rowCount];
}


-(void)saveGrid:(bool)asTemplate
{
    if ([self hasBeenTouched] == true)
    {
        NSString* extension = @"grid";
        if(asTemplate == true)
        {
            extension = @"tmpl";
            //give unique template ids
            gridId = [NSUUID UUID];
            dateCreated = [NSDate date];
            dateModified = [NSDate date];
        }
        NSString* fileName = [self getFilename:extension];

        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *cachePath = [paths objectAtIndex:0];
        BOOL isDir = NO;
        NSError *error;
        if (! [[NSFileManager defaultManager] fileExistsAtPath:cachePath isDirectory:&isDir] && isDir == NO)
        {
            [[NSFileManager defaultManager] createDirectoryAtPath:cachePath withIntermediateDirectories:NO attributes:nil error:&error];
        }
        
        NSString *filePath =  [cachePath stringByAppendingPathComponent:fileName];
        
        dateModified = [NSDate date];
        
        NSArray* gridPixels = [self getCurrentPixelArray];
    
        NSDictionary* dictionary = @{ @"id"     : [gridId UUIDString],
                                      @"startColumn" : [NSNumber numberWithInt:startColumn],
                                      @"startRow" : [NSNumber numberWithInt:startRow],
                                      @"columnCount" : [NSNumber numberWithInt:columnCount],
                                      @"rowCount" : [NSNumber numberWithInt:rowCount],
                                      @"maskId" : [NSNumber numberWithInt:maskId],
                                      @"landscape" : [NSNumber numberWithBool:landscape],
                                      @"pixels" : gridPixels,
                                      @"dateCreated" : dateCreated,
                                      @"dateModified" : dateModified
                                      };

        [dictionary writeToFile:filePath atomically:true];
        
        //create thumbnail
        [self saveToImage:asTemplate toPhotos:false withPixels:gridPixels fileName:(NSString*)fileName];
    }
}

-(NSString*)getFilename:(NSString*)extension
{
    return [NSString stringWithFormat:@"pixelgrid-%@.%@", [gridId UUIDString], extension];
}

-(void)drawCode:(CGContextRef)ctx code:(NSString*)code rect:(CGRect)rect
{
    UIGraphicsBeginImageContext(CGSizeMake(rect.size.width, rect.size.height));
    
    CGContextRef imageCtx = UIGraphicsGetCurrentContext();
    
    // Flip the context because UIKit coordinate system is upside down to Quartz coordinate system
    CGContextTranslateCTM(imageCtx, 0.0, rect.size.height);
    CGContextScaleCTM(imageCtx, 1.0, -1.0);
    
    NSMutableParagraphStyle* textStyle = NSMutableParagraphStyle.defaultParagraphStyle.mutableCopy;
    textStyle.alignment = NSTextAlignmentCenter;
    
    float largestFontSize = 72;
    while ([@"WW" sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:largestFontSize]}].width > rect.size.width)
    {
        largestFontSize--;
    }
    UIFont* font = [UIFont systemFontOfSize:largestFontSize];
    
    NSDictionary* textFontAttributes = @{NSFontAttributeName: font, NSForegroundColorAttributeName: UIColor.lightGrayColor, NSParagraphStyleAttributeName: textStyle};
    [code drawInRect: CGRectMake(0, 0, rect.size.width, rect.size.height) withAttributes: textFontAttributes];
    UIImage* textImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    CGContextDrawImage(ctx, rect, textImage.CGImage);
}

-(bool)hasBeenTouched
{
    bool touched = false;
    for(int pixel=0; pixel<pixels.count; pixel++)
    {
        PixelView* pixelView = [pixels objectAtIndex:pixel];
        touched = [pixelView getTouched];
        
        if(touched == true)
        {
            break;
        }
    }
    
    return  touched;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    touchTimestamp = [NSDate date];
    NSSet *allTouches = [event touchesForView:self];
    
    if([allTouches count] > 1)
    {
        UITouch* t0 = [[allTouches allObjects] objectAtIndex:0];
        UITouch* t1 = [[allTouches allObjects] objectAtIndex:1];
        
        if(firstTouch == t0)
        {
            secondTouch = t1;
        }
        else
        {
            secondTouch = t0;
        }
        
        if(currentPixelView == NULL)
        {
            CGPoint firstPoint = [secondTouch locationInView:self];
            currentPixelView = [self findPixel:firstPoint.x y:firstPoint.y];
        }
        
        if (currentPixelView != NULL)
        {
            if([currentPixelView getTouched] == FALSE)
            {
                [currentPixelView showPixel];
            }
            
            CGPoint secondPoint = [secondTouch locationInView:self];
            [currentPixelView beginValueChange:secondPoint.x y:secondPoint.y];
        }
    }
    else if([allTouches count] > 0)
    {
        firstTouch = [[allTouches allObjects] objectAtIndex:0];
        CGPoint point = [firstTouch locationInView:self];
        
        currentPixelView = [self findPixel:point.x y:point.y];
        if (currentPixelView != NULL)
        {
            if([currentPixelView getTouched] == FALSE)
            {
                [currentPixelView showPixel];
            }
            
            [currentPixelView setWaitingHueChange:true];
        }
    }

}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    NSTimeInterval touchDuration = [[NSDate date] timeIntervalSinceDate:touchTimestamp];
    
    NSSet *allTouches = [event touchesForView:self];

    if([allTouches count] > 1)
    {
        CGPoint point = [secondTouch locationInView:self];
        
        if (currentPixelView != NULL && touchDuration > DELAY_SECONDS)
        {
            [currentPixelView executeValueChange:point.x y:point.y];
        }
    }
    else if([allTouches count] > 0)
    {
        CGPoint point = [firstTouch locationInView:self];
        
        if (currentPixelView != NULL && touchDuration > DELAY_SECONDS)
        {
            if([currentPixelView getWaitingHueChange] == TRUE)
            {
                [currentPixelView setWaitingHueChange:FALSE];
                [currentPixelView beginHueChange:point.x y:point.y];
            }
            else
            {
                [currentPixelView executeHueChange:point.x y:point.y];
            }
        }
    }

}

-(void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self touchesEnded:touches withEvent:event];
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    NSTimeInterval touchDuration = [[NSDate date] timeIntervalSinceDate:touchTimestamp];
    
    NSSet *allTouches = [event touchesForView:self];
    
    if([allTouches count] > 1)
    {
        if (currentPixelView != NULL && touchDuration > 0.0000001)
        {
            [currentPixelView endValueChange];
            //TODO CONFIG:currentHSV = [currentPixelView getHSV];
        }
    }
    else
    {
        if (currentPixelView != NULL)
        {
            //is the pixel 'on', then turn it 'off'
            if([currentPixelView getFirstTouch] == false)
            {
                //check for single tap
                if(touchDuration <= DELAY_SECONDS)
                {
                    [currentPixelView hidePixel];
                }
            }
            else
            {
                //set the hsv to the current hsv, if set
                /* TODO CONFIG: if(currentHSV != NULL)
                {
                    [currentPixelView setHSV:currentHSV[0] saturation:currentHSV[1] value:currentHSV[2]];
                }*/
            }
            
            [currentPixelView endHueChange];

            //TODO CONFIG:currentHSV = [currentPixelView getHSV];
        }
    }
    
    currentPixelView = NULL;
}

-(void)handleDoubleTap:(int)x y:(int)y
{
    currentPixelView = [self findPixel:x y:y];
    if (currentPixelView != NULL)
    {
        if([currentPixelView getTouched] == true)
        {
            [currentPixelView setTouched:false];
            [currentPixelView setHidden:true];
        }
        
        [currentPixelView setWaitingHueChange:true];
    }
}

-(void)handleLongPress:(int)x y:(int)y
{
    currentPixelView = [self findPixel:x y:y];
    if (currentPixelView != NULL)
    {
        if([currentPixelView getTouched] == true)
        {
            [currentPixelView resetHSV];
        }
        
        [currentPixelView setWaitingHueChange:true];
    }
}

-(PixelView*)setCurrentPixel:(int)x y:(int)y
{
    currentPixelView = [self findPixel:x y:y];
    
    return currentPixelView;
}

-(void)setCurrentPixelHue:(float)hue saturation:(float)saturation value:(float)value
{
    if(currentPixelView != NULL)
    {
        //if black - special case
        if(hue == 0 && saturation == 0 && value == 0)
        {
            [currentPixelView unSetHSV];
        }
        else
        {
            [currentPixelView setTouched:true];
            [currentPixelView setHidden:false];
            [currentPixelView setHSV:hue saturation:saturation value:value];
            //TODO CONFIG:currentHSV = [currentPixelView getHSV];
        }
    }
}

-(PixelView*)findPixel:(int) x y:(int) y
{
    PixelView* hitView = NULL;
    
    for(int i=0; i<[pixels count]; i++)
    {
        PixelView* pixelView = [pixels objectAtIndex:i];
        
        CGRect frame = [self convertRect:pixelView.frame fromView:self];
        
        CGPoint point = CGPointMake(x, y);
        if(CGRectContainsPoint(frame, point) == TRUE)
        {
            hitView = pixelView;
        }
    }
    
    return hitView;
}

-(void)changeMask
{
    maskId++;
    maskId %= 3;
    
    [self executeMask];
}

-(UIImage*)getMaskImage
{
    UIImage* maskImage = NULL;
    if(maskId == 1)
    {
        maskImage = [UIImage imageNamed:@"circle_mask"];
    }
    else if(maskId == 2)
    {
        maskImage = [UIImage imageNamed:@"circle_mask_small"];
    }
    return maskImage;
}

-(void)executeMask
{
    UIImage* maskImage = [self getMaskImage];
    
    for(int i=0; i<[pixels count]; i++)
    {
        PixelView* pixelView = [pixels objectAtIndex:i];
        [pixelView setMaskImage:maskImage];
    }
}

-(bool)deleteGrid
{
    bool success = false;
    if(gridId != NULL)
    {
        NSString* fileName = [self getFilename:@"grid"];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *cachePath = [paths objectAtIndex:0];
        NSString *filePath =  [cachePath stringByAppendingPathComponent:fileName];
        NSError *error;
        success = [[NSFileManager defaultManager] removeItemAtPath:filePath error:&error];
    }
    return success;
}

-(bool)isLandscape
{
    return landscape;
}

-(void)setLandscape:(bool)isLandscape
{
    landscape = isLandscape;
}

-(void)saveToImage:(bool)asTemplate toPhotos:(bool)toPhotos withPixels:(NSArray*)gridPixels fileName:(NSString*)fileName
{
    
    CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
    
    CGFloat gridWidth = MAX(self.frame.size.width,self.frame.size.height);
    CGFloat gridHeight = MIN(self.frame.size.width,self.frame.size.height);
    
    CGFloat bitmapWidth = gridWidth;
    CGFloat bitmapHeight = gridHeight;
    
    if(landscape == false)
    {
        //make sure the context is large enough to rotate the landscape image to portrait
        bitmapHeight = gridWidth;
    }
    
    CGContextRef ctx = CGBitmapContextCreate(nil, bitmapWidth, bitmapHeight, 8, 4*(int)bitmapWidth, colorSpaceRef, kCGImageAlphaPremultipliedLast);
    
    CGContextSetFillColorWithColor(ctx, [[UIColor blackColor] CGColor]);
    CGRect fillRect = CGRectMake(0, 0, bitmapWidth, bitmapHeight);
    CGContextFillRect(ctx, fillRect);
    
    if(landscape == false)
    {
        CGContextTranslateCTM(ctx, gridWidth/2.0, gridWidth/2.0);
        CGContextRotateCTM(ctx, -M_PI_2);
        CGContextTranslateCTM(ctx, -gridWidth/2.0, -gridWidth/2.0);
    }
    
    //change the coordinate so that 0,0 starts at top,left
    CGContextTranslateCTM(ctx, 0.0, bitmapHeight);
    CGContextScaleCTM(ctx, 1.0, -1.0);
    
    if(landscape == false)
    {
        //the image will be right aligned, so we left align it with this operation.
        //note that the y-translation is used since the coordinate system has been altered
        CGContextTranslateCTM(ctx, 0, (gridWidth-gridHeight));
    }
    
    //square pixels (fit to width)
    int pixelWidth = (int)(gridWidth/columnCount);
    int pixelHeight = (int)(gridHeight/rowCount);
    int maxSize = MIN(pixelWidth, pixelHeight);
    pixelWidth = maxSize;
    pixelHeight = maxSize;
    
    CGFloat xOverflow = (gridWidth - (pixelWidth*(int)columnCount));
    CGFloat xOffset = xOverflow/2.0;
    
    CGFloat yOverflow = (gridHeight - (pixelHeight*(int)rowCount));
    CGFloat yOffset = yOverflow/2.0;
    
    UIImage* maskImage = NULL;
    if(maskId == 1)
    {
        maskImage = [UIImage imageNamed:@"circle_mask"];
    }
    else if(maskId == 2)
    {
        maskImage = [UIImage imageNamed:@"circle_mask_small"];
    }
    
    if(maskImage != NULL)
    {
        //resize the image to the pixel size
        UIGraphicsBeginImageContext(CGSizeMake(pixelWidth, pixelHeight));
        [maskImage drawInRect:CGRectMake(0, 0, pixelWidth, pixelHeight)];
        maskImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    //assume landscape pixel order of operation...the context translations will account for any
    //changes necessary for portrait
    for (int r = startRow; r < rowCount; r++)
    {
        for (int c = startColumn; c < columnCount; c++)
        {
            CGRect pixelFrame = CGRectMake((pixelWidth*c)+xOffset, (pixelHeight*r)+yOffset, pixelWidth, pixelHeight);
            
            int index = (r*MAX_WORKING_PIXELS_COLUMNS)+c;
            
            NSDictionary* pixelDict = [gridPixels objectAtIndex:index];
            float hue = [[pixelDict objectForKey:@"hue"] floatValue];
            float saturation = [[pixelDict objectForKey:@"saturation"] floatValue];
            float value = [[pixelDict objectForKey:@"value"] floatValue];
            bool touched = [[pixelDict objectForKey:@"touched"] boolValue];
            NSString* code = [pixelDict objectForKey:@"code"];
            
            if(touched == true)
            {
                if(asTemplate == true)
                {
                    code = [PixelView getCode:hue saturation:saturation value:value];
                    [self drawCode:ctx code:code rect:pixelFrame];
                }
                else
                {
                    CGContextSetBlendMode(ctx, kCGBlendModeNormal);
                    hue = (float)[PixelView normalizeToScale:hue A:0 B:360.0 C:0 D:1.0];
                    UIColor* color = [UIColor colorWithHue:hue saturation:saturation brightness:value alpha:1];
                    CGContextSetFillColorWithColor(ctx, [color CGColor]);
                    CGContextFillRect(ctx, pixelFrame);
                    
                    if(maskImage != NULL)
                    {
                        CGContextSetBlendMode(ctx, kCGBlendModeLuminosity);
                        CGContextDrawImage(ctx, pixelFrame, maskImage.CGImage);
                    }
                }
            }
            else if(code != NULL && code.length > 0)
            {
                [self drawCode:ctx code:code rect:pixelFrame];
            }
        }
    }
    
    CGImageRef cgImage = CGBitmapContextCreateImage(ctx);
    
    if(landscape == false)
    {
        //portrait
        CGRect portraitRect = CGRectMake(0, 0, gridHeight, gridWidth);
        CGImageRef cgImagePortrait = CGImageCreateWithImageInRect(cgImage, portraitRect);
        CGImageRelease(cgImage);
        cgImage = cgImagePortrait;
    }
    
    UIImage *image = [UIImage imageWithCGImage:cgImage];
    
    CGImageRelease(cgImage);
    CGContextRelease(ctx);
    
    if(toPhotos == true)
    {
        UIImageWriteToSavedPhotosAlbum(image, NULL, NULL, NULL);
    }
    else
    {
        NSArray* paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString* saveToPath = [paths objectAtIndex:0];
        
        NSString* templateString = @"";
        if(asTemplate == true)
        {
            templateString = @"-tmpl";
        }
        NSString* thumbFileName = [NSString stringWithFormat:@"pixelgrid-%@%@.png", [gridId UUIDString], templateString];
        NSString* thumbFilePath =  [saveToPath stringByAppendingPathComponent:thumbFileName];
        
        bool success = [UIImagePNGRepresentation(image) writeToFile:thumbFilePath atomically:NO];
        
        NSLog(@"saved %d", success);
    }
}

-(void)saveToPhotos
{
    NSArray* gridPixels = [self getCurrentPixelArray];
    [self saveToImage:false toPhotos:true withPixels:gridPixels fileName:NULL];
}

-(NSArray*)getCurrentPixelArray
{
    NSMutableArray* gridPixels = [[NSMutableArray alloc] init];
    for(int pixel=0; pixel<pixels.count; pixel++)
    {
        PixelView* pixelView = [pixels objectAtIndex:pixel];
        NSMutableDictionary* pixel = [[NSMutableDictionary alloc] init];
        
        CGFloat* hsv = [pixelView getHSV];
        
        //if this grid wsa open from a template, save the codes
        NSString* code = [pixelView getPixelCode];
        if(code == NULL)
        {
            code = @"";
        }
        
        [pixel setObject:[NSNumber numberWithFloat:hsv[0]] forKey:@"hue"];
        [pixel setObject:[NSNumber numberWithFloat:hsv[1]] forKey:@"saturation"];
        [pixel setObject:[NSNumber numberWithFloat:hsv[2]] forKey:@"value"];
        [pixel setObject:code forKey:@"code"];
        [pixel setObject:[NSNumber numberWithBool:[pixelView getTouched]] forKey:@"touched"];
        
        [gridPixels addObject:pixel];
    }
    
    return gridPixels;
}
@end
