//
//  PixelView.m
//  PixelBright
//
//  Created by Frank Ayars on 8/4/17.
//  Copyright © 2017 Plain Joe Studio. All rights reserved.
//

#import "PixelView.h"

#define toDegrees(radians) ((radians*180.0)/M_PI)

@implementation PixelView


-(void)initPixelView
{
    [self setUserInteractionEnabled:FALSE];
    
    startWhite = 330.0;
    endWhite = 360.0;
    rangeWhite = (endWhite-startWhite);
    fullWhite = endWhite - (rangeWhite/2.0);
    
    hue = fullWhite;
    saturation = 0.0;
    value = 1.0;
    
    //lock in default (for reset)
    hsvBase[0] = hue;
    hsvBase[1] = saturation;
    hsvBase[2] = value;
    
    hStart = fullWhite;
    hTemp = fullWhite;
    
    vStart = 1.0;
    vTemp = 1.0;
    
    vDirection = 1.0;
    
    //Drawable mask;
    
    isTouched = false;
    isFirstTouch = false;
    
    UIImage *maskImage = [UIImage imageNamed:@"circle_mask"];
    maskView = [[UIImageView alloc] initWithImage:maskImage];
    maskView.contentMode = UIViewContentModeScaleAspectFit;
    maskView.frame = [self bounds];
    [self addSubview:maskView];
    
    [self setHSV:hue saturation:saturation value:value];
    
    [self setBackground];
    
    [self setHidden:TRUE];
}

-(void)layoutSubviews
{
    maskView.frame = self.bounds;
    if(codeLabel != NULL)
    {
        codeLabel.frame = self.bounds;
        [self setCodeLabelFont];
    }
}

/* draw mask
@Override
public void draw(Canvas canvas)
{
    super.draw(canvas);
    
    if(mask != null)
    {
        Rect imageBounds = canvas.getClipBounds();  // Adjust this for where you want it
        
        mask.setBounds(imageBounds);
        mask.draw(canvas);
    }
}*/

-(void) beginHueChange:(int) x y:(int)y
{
    [self hideCodeLabel];
    
    hxStart = x;
    hyStart = y;
    hStart = hue;
    
    isTouched = true;
}

-(void) endHueChange
{
    hue = hTemp;
    isFirstTouch = false;
}

-(void) executeHueChange:(int) x y:(int) y
{
    if([self isDefaultHSV] == true)
    {
        //when first change the hue (from default white), we bring the value down.
        //this is done so that when a user two-finger gestures to change the
        //appearant brightness (change of 'value'), then they can go up or
        //down and recognize a change. We start at 70% value so it can
        //be changed in either direction and yet, initially, be somewhat bright.
        value = 0.7;
        vStart = value;
        hsv[2] = value;
    }
    
    int xDelta = x - hxStart;
    int yDelta = hyStart - y;
    
    float xWeight = 1.0f;
    float yWeight = 1.0f;
    float angle = getSwipeAngle(xDelta, yDelta);
    
    if(angle >= 0.0 && angle <= 90.0)
    {
        yWeight = (float)[PixelView normalizeToScale:angle A:0 B:90 C:0 D:1];
        xWeight = 1.0f - yWeight;
    }
    else if(angle > 90.0 && angle <= 180.0)
    {
        xWeight = (float)[PixelView normalizeToScale:angle A:90 B:180 C:0 D:1];
        yWeight = 1.0f - xWeight;
    }
    else if(angle > 180.0 && angle <= 270.0)
    {
        yWeight = (float)[PixelView normalizeToScale:angle A:180 B:270 C:0 D:1];
        xWeight = 1.0f - yWeight;
    }
    else if(angle > 270.0 && angle < 360.0)
    {
        xWeight = (float)[PixelView normalizeToScale:angle A:270 B:360 C:0 D:1];
        yWeight = 1.0f - xWeight;
    }
    
   float magnitude = (xDelta*xWeight) + (yDelta*yWeight);
    
    //float magnitude = (float) Math.sqrt(Math.pow(xDelta, 2) + Math.pow(yDelta, 2));
   magnitude = (float) [PixelView normalizeToScale:magnitude A:0 B:1000 C:0 D:360];
    
    hTemp = hStart + magnitude;
    hTemp = fmod(hTemp, 360.0);
    
    if(hTemp < 0)
    {
        hTemp = 360.0;
        hStart = 360.0;
    }
    
    //may be coming from white
    saturation = 1.0;
    hsv[1] = saturation;
    
    hsv[0] = hTemp;
    
    [self setBackground];
}

-(void) beginValueChange:(int) x y:(int) y
{
    vxStart = x;
    vyStart = y;
    vStart = value;
    
    isTouched = true;
}

-(void) endValueChange
{
    value = vTemp;
}

-(void) executeValueChange:(int) x y:(int) y
{
    if(isTouched == true)
    {
        int xDelta = x - vxStart;
        int yDelta = vyStart - y;
        
        float xWeight = 1.0f;
        float yWeight = 1.0f;
        float angle = getSwipeAngle(xDelta, yDelta);
        
        if(angle >= 0.0 && angle <= 90.0)
        {
            yWeight = (float)[PixelView normalizeToScale:angle A:0 B:90 C:0 D:1];
            xWeight = 1.0 - yWeight;
        }
        else if(angle > 90.0 && angle <= 180)
        {
            xWeight = (float)[PixelView normalizeToScale:angle A:90 B:180 C:0 D:1];
            yWeight = 1.0 - xWeight;
        }
        else if(angle > 180.0 && angle <= 270.0)
        {
            yWeight = (float)[PixelView normalizeToScale:angle A:180 B:270 C:0 D:1];
            xWeight = 1.0 - yWeight;
        }
        else if(angle > 270.0 && angle < 360.0)
        {
            xWeight = (float)[PixelView normalizeToScale:angle A:270 B:360 C:0 D:1];
            yWeight = 1.0 - xWeight;
        }
        
        float magnitude = (xDelta*xWeight) + (yDelta*yWeight);
        
        magnitude = (float) [PixelView normalizeToScale:magnitude A:0 B:500 C:0 D:1];
        
        vTemp = vStart + magnitude;
        vTemp = MIN(vTemp, 1.0);
        vTemp = MAX(vTemp, 0.1);
        
        hsv[2] = vTemp;
        
        [self setBackground];
    }
}

-(void) setHSV:(CGFloat) h saturation:(CGFloat) s value:(CGFloat) v
{
    [self hideCodeLabel];
    
    isFirstTouch = false;
    hsv[0] = h;
    hsv[1] = s;
    hsv[2] = v;
    
    saturation = s;
    
    hue = h;
    hStart = h;
    hTemp = h;
    
    value = v;
    vStart = v;
    vTemp = v;
    
    [self setBackground];
}

-(CGFloat*) getHSV
{
    return hsv;
}

-(void) resetHSV
{
    hsv[0] = hsvBase[0];
    hsv[1] = hsvBase[1];
    hsv[2] = hsvBase[2];
    
    saturation = hsv[1];
    
    hue = hsvBase[0];
    hStart = hsvBase[0];
    hTemp = hsvBase[0];
    
    value = hsvBase[2];
    vStart = hsvBase[2];
    vTemp = hsvBase[2];
    
    [self setBackground];
}

-(bool) isDefaultHSV
{
    bool isDefault = false;
    
    if(hsv[0] == hsvBase[0] && hsv[1] == hsvBase[1] && hsv[2] == hsvBase[2])
    {
        isDefault = true;
    }
    
    return isDefault;
}

-(void) unSetHSV
{
    [self resetHSV];
    isTouched = false;
    [self setHidden:TRUE];
}

-(void) setTouched:(bool) t
{
    isTouched = t;
    timestamp = [NSDate date];
    isFirstTouch = t;
}

-(bool) getTouched
{
    return isTouched;
}


-(void) setMaskImage:(UIImage*)maskImage
{
    
    if(maskImage == NULL)
    {
        [maskView setHidden:TRUE];
    }
    else
    {
        maskView.image = maskImage;
        [maskView setHidden:FALSE];
    }
}

-(NSDate*) getTimestamp
{
    return timestamp;
}

-(bool) getWaitingHueChange
{
    return isWaitingHueChange;
}

-(void) setWaitingHueChange:(bool) waitingHueChange
{
    isWaitingHueChange = waitingHueChange;
}

-(bool) getFirstTouch
{
    return isFirstTouch;
}

-(void) setBackground
{
    float h = (float)[PixelView normalizeToScale:hsv[0] A:0 B:360.0 C:0 D:1.0];
    //NSLog(@"final hue %f", h);
    UIColor* color = [UIColor colorWithHue:h saturation:hsv[1] brightness:hsv[2] alpha:1];
    [self setBackgroundColor:color];
}

float getSwipeAngle(float xDelta, float yDelta)
{
    float angle;
    
    angle = (float)atan2(yDelta, xDelta);

    angle = (float)toDegrees(angle);
    if(angle < 0)
    {
        angle += 360.0;
    }
    return angle;
}

-(void) setPauseTouches:(bool)pause
{
    pauseTouches = pause;
}

-(bool) getPauseTouches
{
    return pauseTouches;
}

-(void) setCode:(CGFloat) h saturation:(CGFloat) s value:(CGFloat) v
{
    pixelCode = [PixelView getCode:h saturation:s value:v];
    [self showPixelCode];
}

-(void) showPixelCode
{
    if(pixelCode != NULL)
    {
        codeLabel = [[UILabel alloc] initWithFrame:self.bounds];
        codeLabel.textAlignment = NSTextAlignmentCenter;
        [codeLabel setText:pixelCode];
        codeLabel.textColor = [UIColor lightGrayColor];
        //[codeLabel setFont:[UIFont boldSystemFontOfSize:72]];
        [codeLabel setBackgroundColor:[UIColor blackColor]];
        [self setCodeLabelFont];
        displayingCode = true;
        [self addSubview:codeLabel];
    }
}

-(void) setCodeLabelFont
{
    codeLabel.numberOfLines = 1;
    float largestFontSize = 72;
    while ([@"WW" sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:largestFontSize]}].width > codeLabel.frame.size.width)
    {
        largestFontSize--;
    }
    codeLabel.font = [UIFont systemFontOfSize:largestFontSize];
}

+(NSString*)getCode:(CGFloat)h saturation:(CGFloat)s value:(CGFloat)v
{
    NSString* code = NULL;
    if(s == 0.0 && v == 1.0)//white
    {
        code = @"W";
    }
    else if((h > 340 && h <= 360) || (h >= 0 && h <= 10))
    {
        code = @"R";
    }
    else if(h > 10 && h <= 30)
    {
        code = @"RO";
    }
    else if(h > 30 && h <= 45)
    {
        code = @"O";
    }
    else if(h > 45 && h <= 55)
    {
        code = @"YO";
    }
    else if(h > 55 && h <= 70)
    {
        code = @"Y";
    }
    else if(h > 70 && h <= 100)
    {
        code = @"YG";
    }
    else if(h > 100 && h <= 130)
    {
        code = @"G";
    }
    else if(h > 140 && h <= 190)
    {
        code = @"GB";
    }
    else if(h > 190 && h <= 230)
    {
        code = @"B";
    }
    else if(h > 230 && h <= 260)
    {
        code = @"BV";
    }
    else if(h > 230 && h <= 285)
    {
        code = @"V";
    }
    else if(h > 285 && h <= 340)
    {
        code = @"VR";
    }
    
    return code;
}

-(void)showPixel
{
    self.hidden = false;
    [self setTouched:true];
    
    [self hideCodeLabel];
}

-(void)hidePixel
{
    [self setTouched:false];
    if(codeLabel != NULL)
    {
        displayingCode = true;
        //if from template, show the code label
        [self addSubview:codeLabel];
    }
    else
    {
        self.hidden = true;
    }
}

-(void)hideCodeLabel
{
    //if code label is present (opened from template), remove it
    if(codeLabel != NULL)
    {
        displayingCode = false;
        [codeLabel removeFromSuperview];
    }
}

-(bool) isDisplayingCode
{
    return displayingCode;
}

-(NSString*)getPixelCode
{
    return pixelCode;
}

-(void)setPixelCode:(NSString*)code
{
    pixelCode = code;
    [self showPixelCode];
}

+(double) normalizeToScale:(double) val A:(double) A B:(double) B C:(double) C D:(double) D
{
    double newVal;
    
    //Old Scale: A - B
    //New Scale: C - D
    
    newVal = C + (val-A)*(D-C)/(B-A);
    
    return newVal;
}

@end
