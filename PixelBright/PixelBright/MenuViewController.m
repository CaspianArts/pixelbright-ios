//
//  TemplatesCollectionView.m
//  PixelBright
//
//  Created by Frank Ayars on 10/31/17.
//  Copyright © 2017 Plain Joe Studio. All rights reserved.
//

#import "MenuViewController.h"
#import "ViewController.h"

@implementation MenuViewController

@synthesize controller;

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(menuOptions == NULL)
    {
        menuOptions = [[NSMutableArray alloc] init];
        [menuOptions addObject:@"Zoom 1"];
        [menuOptions addObject:@"Zoom 2x1"];
        [menuOptions addObject:@"Zoom 3x2"];
        [menuOptions addObject:@"Zoom 6x4"];
        [menuOptions addObject:@"Zoom 12x8"];
        [menuOptions addObject:@"Zoom 24x16"];
        [menuOptions addObject:@"New 1"];
        [menuOptions addObject:@"New 2x1"];
        [menuOptions addObject:@"New 3x2"];
        [menuOptions addObject:@"New 6x4"];
        [menuOptions addObject:@"New 12x8"];
        [menuOptions addObject:@"New 24x16"];
    }
    
    return menuOptions.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    UILabel* title = (UILabel *)[cell viewWithTag:100];
    [title setText:[menuOptions objectAtIndex:indexPath.row]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    long menuItem = indexPath.row;
    int columns = 1;
    int rows = 1;
    bool zoom = true;
    
    [menuOptions addObject:@"Zoom 1"];
    [menuOptions addObject:@"Zoom 2x1"];
    [menuOptions addObject:@"Zoom 3x2"];
    [menuOptions addObject:@"Zoom 6x4"];
    [menuOptions addObject:@"Zoom 12x8"];
    [menuOptions addObject:@"Zoom 24x16"];
    [menuOptions addObject:@"New 1"];
    [menuOptions addObject:@"New 2x1"];
    [menuOptions addObject:@"New 3x2"];
    [menuOptions addObject:@"New 6x4"];
    [menuOptions addObject:@"New 12x8"];
    [menuOptions addObject:@"New 24x16"];
    
    if(menuItem == 0 || menuItem == 6)
    {
        columns = 1;
        rows = 1;
    }
    else if(menuItem == 1 || menuItem == 7)
    {
        columns = 2;
        rows = 1;
    }
    else if(menuItem == 2 || menuItem == 8)
    {
        columns = 3;
        rows = 2;
    }
    else if(menuItem == 3 || menuItem == 9)
    {
        columns = 6;
        rows = 4;
    }
    else if(menuItem == 4 || menuItem == 10)
    {
        columns = 12;
        rows = 8;
    }
    else if(menuItem == 5 || menuItem == 11)
    {
        columns = 24;
        rows = 16;
    }
    
    if(menuItem > 5)
    {
        zoom = false;
    }
    
    if(zoom == true)
    {
        [controller adaptGridViewToColumns:columns rows:rows];
    }
    else
    {
        [controller newGridViewWithColumns:columns rows:rows];
    }
    [controller closeMenu:self];
}

@end
