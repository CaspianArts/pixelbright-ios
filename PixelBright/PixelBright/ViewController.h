//
//  ViewController.h
//  PixelBright
//
//  Created by Frank Ayars on 8/4/17.
//  Copyright © 2017 Plain Joe Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PixelGridView.h"
#import "MenuView.h"
#import "ContextMenuView.h"
#import "SavedView.h"
#import "ColorView.h"
#import "TemplatesView.h"
#import "SavedCollectionViewController.h"
#import "TemplatesCollectionViewController.h"
#import "ContextMenuViewController.h"
#import "MenuViewController.h"
#import <Social/Social.h>
#import <CoreMotion/CoreMotion.h>

#define radiansToDegrees(x) (180/M_PI)*x

@interface ViewController : UIViewController
{
    IBOutlet PixelGridView* pixelGridView;
    IBOutlet MenuView* menuView;
    IBOutlet ContextMenuView* contextMenuView;
    IBOutlet SavedView* savedView;
    IBOutlet ColorView* colorView;
    IBOutlet TemplatesView* templatesView;
    
    IBOutlet UIButton* maskButton;
    IBOutlet UIButton* gridSize;
    IBOutlet UIButton* menuButton;
    
    SavedCollectionViewController* savedCollectionViewController;
    TemplatesCollectionViewController* templatesCollectionViewController;
    MenuViewController* menuViewController;
    ContextMenuViewController* contextMenuViewController;
    
    CMMotionManager* motionManager;
    float calibrationAngle;
    
    IBOutlet UIView* header;
    
    CGFloat rotationOffset;
}

@property (nonatomic) UIInterfaceOrientation initialOrientation;
@property (nonatomic) UIInterfaceOrientation currentOrientation;

-(IBAction)changeMask:(id)sender;
-(IBAction)menu:(id)sender;
-(IBAction)closeMenu:(id)sender;
-(IBAction)openSaved:(id)sender;
-(IBAction)closeSaved:(id)sender;
-(IBAction)deleteGrid:(id)sender;
-(IBAction)closeContextMenu:(id)sender;
-(IBAction)saveAsTemplate:(id)sender;
-(IBAction)openTemplates:(id)sender;
-(IBAction)closeTemplates:(id)sender;

-(void)adaptGridViewToColumns:(int)columns rows:(int)rows;
-(void)newGridViewWithColumns:(int)columns rows:(int)rows;
-(void)openGrid:(NSString*)fileName;
-(void)handleDoubleTap:(UITapGestureRecognizer *)recognizer;
-(void)handleLongPress:(UITapGestureRecognizer *)recognizer;
-(void)setGridSize:(int)columnCount rowCount:(int)rowCount;
-(void)setCurrentPixelHue:(float)hue saturation:(float)saturation value:(float)value;
-(void)share;
-(void)closeColors;
-(bool)isLandscape;
-(void)setLandscape:(bool)isLandscape;
-(void)saveToPhotos;

@end

