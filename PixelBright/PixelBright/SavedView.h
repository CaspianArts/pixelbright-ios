//
//  SavedView.h
//  PixelBright
//
//  Created by Frank Ayars on 10/30/17.
//  Copyright © 2017 Plain Joe Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface SavedView : UIView
{
    IBOutlet ViewController* controller;
    IBOutlet UIView* collectionContainer;
    IBOutlet UIImageView* savedLabel;
    IBOutlet UIView* bg;
    CAGradientLayer *gradient;
}

-(void)initSavedView;

@end
