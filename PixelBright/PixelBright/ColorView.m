//
//  ColorView.m
//  PixelBright
//
//  Created by Frank Ayars on 12/20/17.
//  Copyright © 2017 Plain Joe Studio. All rights reserved.
//

#import "ColorView.h"
#import "ViewController.h"

@implementation ColorView
@synthesize controller;

-(void)pickColor:(int)x y:(int)y
{
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    unsigned char pixel[4] = {0};
    
    CGContextRef context = CGBitmapContextCreate(pixel, 1, 1, 8, 4, colorSpace, kCGBitmapAlphaInfoMask & kCGImageAlphaPremultipliedLast);
    
    CGContextTranslateCTM(context, -x, -y);
    
    [self.layer renderInContext:context];
    
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    //NSLog(@"pixel: %d %d %d %d", pixel[0], pixel[1], pixel[2], pixel[3]);
    
    CGFloat r = pixel[0];
    CGFloat g = pixel[1];
    CGFloat b = pixel[2];
    
    
    UIColor *color = [UIColor colorWithRed:r/255.0 green:g/255.0 blue:pixel[2]/255.0 alpha:b/255.0];
        
    CGFloat h;
    CGFloat s;
    CGFloat v;
    CGFloat a;

    [color getHue:&h saturation:&s brightness:&v alpha:&a];
    NSLog(@"%f %f %f", h*100, s, v);
    
    if(v > 0.5) //don't get background
    {
        [controller setCurrentPixelHue:h*360 saturation:s value:v];
    }
    
    [controller closeColors];
}

/*
-(void)layoutSubviews
{
    CGFloat width = controller.view.frame.size.width;
    CGFloat height = controller.view.frame.size.height;
    if(UIInterfaceOrientationIsLandscape(controller.currentOrientation) == true)
    {
        width = MAX(controller.view.frame.size.width, controller.view.frame.size.height);
        height = MIN(controller.view.frame.size.width, controller.view.frame.size.height);
        
        CGFloat left = 20;
        CGFloat top = ((height - colorWheel.frame.size.height)/2.0);
        colorWheel.frame = CGRectMake(left, top, colorWheel.frame.size.width, colorWheel.frame.size.height);
        
        left = colorWheel.frame.origin.x + colorWheel.frame.size.width + 20;
        top = ((height - borderForBW.frame.size.height)/2.0);
        borderForBW.frame = CGRectMake(left, top, borderForBW.frame.size.width, borderForBW.frame.size.height);
    }
    else
    {
        width = MIN(controller.view.frame.size.width, controller.view.frame.size.height);
        height = MAX(controller.view.frame.size.width, controller.view.frame.size.height);
        
        CGFloat left = ((width - colorWheel.frame.size.width)/2.0);
        CGFloat top = 72;
        colorWheel.frame = CGRectMake(left, top, colorWheel.frame.size.width, colorWheel.frame.size.height);
        
        left = ((width - borderForBW.frame.size.width)/2.0);
        top = colorWheel.frame.origin.y + colorWheel.frame.size.height + 20;
        borderForBW.frame = CGRectMake(left, top, borderForBW.frame.size.width, borderForBW.frame.size.height);
    }
}
*/

-(void)position:(int)x y:(int)y
{
    //x, y - are center point (by default) of the pixel to be colored
    int halfWidth = colorWheel.frame.size.width/2.0;
    int halfHeight = colorWheel.frame.size.height/2.0;
    
    int viewWidth = MIN(controller.view.frame.size.width, controller.view.frame.size.height);
    int viewHeight = MAX(controller.view.frame.size.width, controller.view.frame.size.height);
    
    if(UIInterfaceOrientationIsLandscape(controller.currentOrientation) == true)
    {
        viewWidth = MAX(controller.view.frame.size.width, controller.view.frame.size.height);
        viewHeight = MIN(controller.view.frame.size.width, controller.view.frame.size.height);
    }
    
    x = x - halfWidth;
    y = y - halfHeight;
    
    if(x < 0)
    {
        //left align
        x = 0;
    }
    
    if(x + (halfWidth*2) > viewWidth)
    {
        //right align
        x = viewWidth - colorWheel.frame.size.width;
    }
    
    if(y < 0)
    {
        //top align
        y = 0;
    }
    
    if(y + (halfHeight*2) > viewHeight)
    {
        //bottom align
        y = viewHeight - colorWheel.frame.size.height;
    }
    
    colorWheel.frame = CGRectMake(x, y, colorWheel.frame.size.width, colorWheel.frame.size.height);
}

@end
