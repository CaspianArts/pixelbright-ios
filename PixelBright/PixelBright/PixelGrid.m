//
//  PixelGrid.m
//  PixelBright
//
//  Created by Frank Ayars on 8/4/17.
//  Copyright © 2017 Plain Joe Studio. All rights reserved.
//

#import "PixelGrid.h"

@implementation PixelGrid

- (id) init
{
    self = [super init];
    if (self != nil)
    {
        gridId = [NSUUID UUID];
    }
    return self;
}

@end
