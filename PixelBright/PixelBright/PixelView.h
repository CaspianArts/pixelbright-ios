//
//  PixelView.h
//  PixelBright
//
//  Created by Frank Ayars on 8/4/17.
//  Copyright © 2017 Plain Joe Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PixelView : UIView
{
    float startWhite;
    float endWhite;
    float rangeWhite;
    float fullWhite;
    
    CGFloat hsvBase[3];
    CGFloat hsv[3];
    
    bool isWaitingHueChange;
    
    int hxStart;
    int hyStart;
    
    int vxStart;
    int vyStart;
    
    //default to white...
    CGFloat hue;
    CGFloat saturation;
    CGFloat value;
    
    CGFloat hStart;
    CGFloat hTemp;
    
    CGFloat vStart;
    CGFloat vTemp;
    
    CGFloat vDirection;
    
    //Drawable mask;
    
    bool isTouched;
    bool isFirstTouch;
    
    UIImageView* maskView;
    
    NSDate* timestamp;
    
    bool pauseTouches;
    
    NSString* pixelCode;
    
    UILabel* codeLabel;
    
    bool displayingCode;
}

+(double) normalizeToScale:(double) val A:(double) A B:(double) B C:(double) C D:(double) D;
-(void) initPixelView;
-(void) setTouched:(bool)t;
-(bool) getFirstTouch;
-(bool) getTouched;
-(bool) isDisplayingCode;
-(void) setWaitingHueChange:(bool) waitingHueChange;
-(bool) getWaitingHueChange;
-(NSDate*) getTimestamp;
-(void) beginHueChange:(int) x y:(int) y;
-(void) executeHueChange:(int) x y:(int) y;
-(void) endHueChange;
-(void) beginValueChange:(int) x y:(int) y;
-(void) executeValueChange:(int) x y:(int) y;
-(void) endValueChange;
-(void) setMaskImage:(UIImage*)maskImage;
-(CGFloat*) getHSV;
-(void) setHSV:(CGFloat) h saturation:(CGFloat) s value:(CGFloat) v;
-(void) setCode:(CGFloat) h saturation:(CGFloat) s value:(CGFloat) v;
-(bool) isDefaultHSV;
-(void) unSetHSV;
-(void) resetHSV;
-(void) setPauseTouches:(bool)pause;
-(bool) getPauseTouches;
-(void) hidePixel;
-(void) showPixel;
-(NSString*)getPixelCode;
-(void)setPixelCode:(NSString*)code;
+(NSString*)getCode:(CGFloat)h saturation:(CGFloat)s value:(CGFloat)v;

@end
